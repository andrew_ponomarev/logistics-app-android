package ua.kharkov.kpi.logisticsapp;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import ua.kharkov.kpi.logisticsapp.ArrayAdapters.CarriersArrayAdapter;


public class CharacteristicCarriersActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristic_carriers);
        Button button =  (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CharacteristicCarriersActivity.this, CharacteristicPerformersStorageActivity.class);
                startActivity(intent);
            }
        });
        String[] values = new String[] { "Перевозчик 1", "Перевозчик 2", "Перевозчик 3",
                "Перевозчик 4", "Перевозчик 5", "Перевозчик 6", "Перевозчик 7", "Перевозчик 8",
                "Перевозчик 9", "Перевозчик 10" };
        CarriersArrayAdapter adapter = new CarriersArrayAdapter(this, values);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_characteristic_carriers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

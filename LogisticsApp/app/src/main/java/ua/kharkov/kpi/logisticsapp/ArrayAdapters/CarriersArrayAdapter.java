package ua.kharkov.kpi.logisticsapp.ArrayAdapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import ua.kharkov.kpi.logisticsapp.R;

public class CarriersArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public CarriersArrayAdapter(Context context, String[] values) {
        super(context, R.layout.carriers_rowlayout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.carriers_rowlayout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.textView);
        textView.setText(values[position]);
        EditText costOfRegistrationEditText = (EditText) rowView.findViewById(R.id.costOfRegistrationEditText);
        EditText costOfTripEditText = (EditText) rowView.findViewById(R.id.costOfTripEditText);
        return rowView;
    }
}

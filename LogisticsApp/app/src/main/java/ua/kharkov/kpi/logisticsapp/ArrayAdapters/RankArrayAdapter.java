package ua.kharkov.kpi.logisticsapp.ArrayAdapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import ua.kharkov.kpi.logisticsapp.R;

public class RankArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public RankArrayAdapter(Context context, String[] values) {
        super(context, R.layout.rank_row_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rank_row_layout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.textView2);
        textView.setText(values[position]);
        EditText rankEditText = (EditText) rowView.findViewById(R.id.editText);
        rankEditText.setHint("Ранг");
        return rowView;
    }
}

package ua.kharkov.kpi.logisticsapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import ua.kharkov.kpi.logisticsapp.ArrayAdapters.CarriersArrayAdapter;
import ua.kharkov.kpi.logisticsapp.ArrayAdapters.StorageArrayAdapter;


public class CharacteristicPerformersStorageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristic_performers_storage);
        Button button =  (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CharacteristicPerformersStorageActivity.this, ExpertsEstimatingActivity.class);
                startActivity(intent);
            }
        });
        String[] values = new String[] { "Склад 1", "Склад 2", "Склад 3",
                "Склад 4", "Склад 5", "Склад 6", "Склад 7", "Склад 8",
                "Склад 9", "Склад 10" };
        StorageArrayAdapter adapter = new StorageArrayAdapter(this, values);
        ListView listView = (ListView) findViewById(R.id.listView2);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_characteristic_performers_storage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
